﻿using CheckOut.Models;

namespace CheckOutTests;

internal class MockPriceList: PriceList
{
    public MockPriceList()
    {
        AddRule('A', 50);
        AddRule('A', 130, 3);

        AddRule('B', 30);
        AddRule('B', 45, 2);

        AddRule('C', 20);
        AddRule('D', 15);
    }
}

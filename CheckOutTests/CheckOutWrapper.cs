﻿using CheckOut.Models;
using System;

namespace CheckOutTests;

internal class CheckOutWrapper : CheckOut.Models.CheckOut
{
    public CheckOutWrapper(IPriceList priceList) : base(priceList)
    {
    }

    public CheckOutWrapper Init(string goodCodes = "")
    {
        ArgumentNullException.ThrowIfNull(nameof(goodCodes));
        _items.Clear();
        foreach (var goodCode in goodCodes)
        {
            Scan(goodCode);
        }
        return this;
    }
}

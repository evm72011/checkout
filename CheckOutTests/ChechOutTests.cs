using CheckOut.Models;
using System;
using Xunit;

namespace CheckOutTests;

public class ChechOutTests
{
    private PriceList _priceList = new MockPriceList();

    [Fact(DisplayName = "Test total")]
    public void TestTotal()
    {
        var checkOut = new CheckOutWrapper(_priceList);
        Assert.Equal(0, checkOut.Total);
        Assert.Equal(50, checkOut.Init("A").Total);
        Assert.Equal(80, checkOut.Init("AB").Total);
        Assert.Equal(115, checkOut.Init("CDBA").Total);

        Assert.Equal(100, checkOut.Init("AA").Total);
        Assert.Equal(130, checkOut.Init("AAA").Total);
        Assert.Equal(180, checkOut.Init("AAAA").Total);
        Assert.Equal(230, checkOut.Init("AAAAA").Total);
        Assert.Equal(260, checkOut.Init("AAAAAA").Total);

        Assert.Equal(160, checkOut.Init("AAAB").Total);
        Assert.Equal(175, checkOut.Init("AAABB").Total);
        Assert.Equal(190, checkOut.Init("AAABBD").Total);
        Assert.Equal(190, checkOut.Init("DABABA").Total);
    }

    [Fact(DisplayName = "Test incremental")]
    public void TestIncremental()
    {
        var checkOut = new CheckOutWrapper(_priceList);
        checkOut.Scan('A');
        Assert.Equal(50, checkOut.Total);
        checkOut.Scan('B');
        Assert.Equal(80, checkOut.Total);
        checkOut.Scan('A');
        Assert.Equal(130, checkOut.Total);
        checkOut.Scan('A');
        Assert.Equal(160, checkOut.Total);
        checkOut.Scan('B');
        Assert.Equal(175, checkOut.Total);
    }

    [Fact(DisplayName = "Unknown good or price rule")]
    public void TestUnknownGood()
    {
        var checkOut = new CheckOutWrapper(_priceList);
        checkOut.Scan('F');
        Assert.Throws<NoPriceRuleException>(() => checkOut.Total);
    }
}

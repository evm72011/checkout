﻿namespace CheckOut.Models;

public class PriceRule
{
    public char GoodCode { get; set; }

    public int Count { get; set; }

    public decimal Price { get; set; }

    public PriceRule(char goodCode, decimal price, int count = 1)
    {
        GoodCode = goodCode;
        Price = price;
        Count = count;
    }
}

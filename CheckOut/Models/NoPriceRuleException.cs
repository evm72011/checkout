﻿namespace CheckOut.Models;

public class NoPriceRuleException: Exception
{
    public NoPriceRuleException(char goodCode, int count)
        :base($"There are no price rule: {goodCode}/{count}")
    {
    }
}

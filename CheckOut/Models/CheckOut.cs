﻿namespace CheckOut.Models;

public class CheckOut
{
    private IPriceList _priceList;

    protected List<CheckOutItem> _items = new();

    public decimal Total
    {
        get => _items.Select(i => _priceList.GetSumm(i.GoodCode, i.Count)).Sum();
    }

    public CheckOut(IPriceList priceList)
    {
        _priceList = priceList;
    }

    public void Scan(char goodCode, int count = 1)
    {
        var item = _items.FirstOrDefault(i => i.GoodCode == goodCode);
        if (item is null)
        {
            item = new CheckOutItem(goodCode, count);
            _items.Add(item);
        }
        else
        {
            item.Count += count;
        }
    }
}

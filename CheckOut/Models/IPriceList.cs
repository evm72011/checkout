﻿namespace CheckOut.Models;

public interface IPriceList
{
    decimal GetSumm(char goodCode, int count);
}

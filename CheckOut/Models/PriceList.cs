﻿namespace CheckOut.Models;

public  class PriceList: IPriceList
{
    private List<PriceRule> _priceRules = new();

    public void AddRule(char goodCode, decimal price, int count = 1)
    {
        var rule = new PriceRule(goodCode, price, count);
        _priceRules.Add(rule);
    }

    public decimal GetSumm(char goodCode, int count)
    {
        var rule = GetPriceRule(goodCode, count);
        if (rule.Count == count)
        {
            return rule.Price;
        }
        else
        {
            return rule.Price + GetSumm(goodCode, count - rule.Count);
        }
    }

    private PriceRule GetPriceRule(char goodCode, int count)
    {
        var goodRules = _priceRules
            .Where(r => r.GoodCode == goodCode && r.Count <= count)
            .OrderBy(r => r.Count)
            .ToList();
        if (goodRules.Any())
        {
            return goodRules.Last();
        }
        throw new NoPriceRuleException(goodCode, count);
    }
}

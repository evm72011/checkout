﻿namespace CheckOut.Models;

public class CheckOutItem
{
    public char GoodCode { get; set; }

    public int Count { get; set; }

    public CheckOutItem(char goodCode, int count = 1)
    {
        GoodCode = goodCode;
        Count = count;
    }
}
